#|
This file is a part of cl-hentai project.
Copyright (c) 2013 Gar Sue (labs.garsue@gmail.com)
|#

(in-package :cl-user)
(defpackage cl-hentai-test-asd
  (:use :cl :asdf))
(in-package :cl-hentai-test-asd)

(defsystem cl-hentai-test
  :author "Gar Sue"
  :license "LLGPL"
  :depends-on (:cl-hentai
               :fiveam)
  :components ((:module "test"
                        :components
                        ((:file "crawler"))))
  :perform (load-op :after (op c) (asdf:clear-system c)))
