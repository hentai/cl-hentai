#|
This file is a part of cl-hentai project.
Copyright (c) 2013 Gar Sue (labs.garsue@gmail.com)
|#

(in-package :cl-user)
(defpackage hentai.crawler
  (:use :cl
        :optima
        :anaphora))
(in-package :hentai.crawler)

;; enable cl-annot
(annot:enable-annot-syntax)

(defconstant +page-url+ "http://hentai4.me/page/~d")
(defconstant +img-url+ "http://~A.hdporn4.me/~A/~A")
(defconstant +metadata-url+ "http://hentai4.me/ajax.php?dowork=getimg&id=~A&host=~A")

(defconstant +img-id-re+ "/gallery-(\\d+)-(\\d+)")
(defconstant +img-name-re+ "\\d+-hentai4.me-(\\d+).jpg")

@export (defparameter *http-request*
          (lambda (url-str &rest args)
            (apply #'drakma:http-request url-str :user-agent :explorer args)))

;;;
;;; Utility
;;;

(defun get-download-path (homedir-getter)
  ;; get-download-path returns path to platform compatible download directory.
  (merge-pathnames ".hentai/" (funcall homedir-getter)))

@export (defparameter *download-path* (get-download-path #'user-homedir-pathname))

@export
(defun get-img-id (path)
  ;; get-img-id returns image id and host id in gallery path.
  (match path
    ((optima.ppcre:ppcre +img-id-re+ img-id host-id)
     (values img-id host-id))
    (otherwise
     (values "" ""))))

;; Intern keyword for json keys
;; Metadata is a hash-table for JSON data which returned by the server
;; encoded in URL encode and base64.
:img
:img-name
:folder-link
:host

(defun create-img-url (metadata img-name)
  ;; create-img-url create direct link to Hentai image from image metadata and image filename.
  (format nil +img-url+
          (gethash :host metadata)
          (gethash :folder-link metadata)
          img-name))

(defun convert-seq-file (img-name)
  ;; convert-seq-file just change file extension to .jpg.
  (match img-name
    ((optima.ppcre:ppcre +img-name-re+ matched)
     (concatenate 'string matched ".jpg"))))

(defun create-filename (metadata img-name)
  ;; create-filename generates pathname of actual destination and return it.
  (aif (convert-seq-file img-name)
       (let* ((filename (concatenate 'string
                                     (write-to-string
                                      (gethash :folder-link metadata))
                                     "/"
                                     it))
              (file-path (merge-pathnames filename *download-path*)))
         (if (wild-pathname-p file-path)
             (log:warn "Skip ~S because of wild pathname." file-path)
             (ensure-directories-exist file-path)))))

(defun convert-numerical-entity (doc)
  (reduce (lambda (doc key)
            (let ((numerical-entity
                   (gethash key resources:*numerical-entity-table*)))
              (cl-ppcre:regex-replace-all key doc numerical-entity)))
          (alexandria:hash-table-keys resources:*numerical-entity-table*)
          :initial-value doc))

;;;
;;; Parse
;;;

(defmacro with-hentai-request ((var url-str &rest args) &body body)
  ;; with-hentai-request creates request context.
  (let ((resp (gensym)))
    `(let ((,resp (multiple-value-list
                   (ignore-errors (funcall *http-request* ,url-str ,@args)))))
       (match ,resp
         ((cons ,var (cons 200 _))
          ,@body)
         ((cons _ (cons e _))
          (log:error (format nil "~A" e))
          nil)
         (otherwise
          (log:error ,resp))))))

@export
(defun get-hentai-links (index)
  ;; get-hentai-links calls get-hentai-href-list and returns links to gallery pages.
  (with-hentai-request (body (format nil +page-url+ index))
                                        ;(log:debug body)
    (get-hentai-href-list body)))

(defun get-hentai-href-list (body)
  ;; get-hentai-link find all links in a overview page.
  (let* ((numerical-entity-body (convert-numerical-entity body))
         (doc (cxml:parse numerical-entity-body (cxml-stp:make-builder)))
         (nodes (xpath:all-nodes
                 (xpath:evaluate "//a[@rel='bookmark']/@href" doc)))
         (links (mapcar #'cxml-stp:value nodes)))
    links))

@export
(defun get-gallery-link (url-str)
  ;; get-gallery-link calls get-gallery-href and returns gallery link.
  (with-hentai-request (body url-str)
    (get-gallery-href body)))

(defun get-gallery-href (body)
  ;; get-gallery-href find all links in a overview page.
  (let* ((numerical-entity-body (convert-numerical-entity body))
         (doc (cxml:parse numerical-entity-body (cxml-stp:make-builder)))
         (node (xpath:first-node
                (xpath:evaluate "//a[@title='View Gallery']/@href" doc)))
         (link (cxml-stp:value node)))
    link))

(defun create-mapped-data (body)
  (ignore-errors
    (let* ((convert (alexandria:compose #'yason:parse
                                        #'cl-base64:base64-string-to-string
                                        #'do-urlencode:urldecode))
           (yason:*parse-object-key-fn*
            (lambda (js-name)
              (or (find-symbol
                   (substitute #\- #\_ (string-upcase js-name))
                   :keyword)
                  js-name))))
      (funcall convert body))))

@export
(defun get-img-metadata (img-id host-id)
  ;; get-img-metadata fetches encoded JSON style metadata with image id and host id
  ;; obtained from gallery page and returns decoded metadata.
  (with-hentai-request (body (format nil +metadata-url+ img-id host-id))
                                        ;(log:debug body)
    (let ((mapped-data (create-mapped-data body)))
      (match mapped-data
        ((cons _ (cons e _))
         (log:error (format nil "~A" e))
         nil)
        (otherwise
                                        ;(log:debug mapped-data)
         mapped-data)))))

;;;
;;; Download
;;;

(defun get-img (metadata img-name out-stream)
  ;; get-img fetches contents and output HTTP response body to out-stream.
  (with-hentai-request (resp-vec (create-img-url metadata img-name) :force-binary t)
    (fast-io:with-fast-output (buffer out-stream)
      (fast-io:fast-write-sequence resp-vec buffer))
    (length resp-vec)))

@export
(defun download-img (metadata img-name)
  ;; download-img downloads an image file on the server and write a file
  ;; named based on image metadata in the default direcotry.
  (with-open-file (out-stream (create-filename metadata img-name)
                              :direction :output :if-exists :supersede
                              :element-type 'unsigned-byte)
    (let ((written (get-img metadata img-name out-stream)))
      (log:info "~A -> ~A bytes written." img-name written))))
