#|
This file is a part of cl-hentai project.
Copyright (c) 2013 Gar Sue (labs.garsue@gmail.com)
|#

(in-package :cl-user)
(defpackage hentai.core
  (:use :cl
        :anaphora
        :hentai.crawler))
(in-package :hentai.core)

;; enable cl-annot
(annot:enable-annot-syntax)

;; Setup Logger
(log:config :info :daily
            (merge-pathnames "log/cl-hentai-%Y%m%d.log"
                             *download-path*))

@export (defparameter *start* 1)
@export (defparameter *end* 1)
@export (defparameter *worker-count* 5)
@export (defparameter *fixed-capacity* 100)

(setf lparallel:*kernel* (lparallel:make-kernel *worker-count*))

(defun download-imgs (metadata)
  (let ((names (gethash :img-name metadata)))
    (dolist (name names)
      (download-img metadata name))))

(defun get-hentai-metadata (link)
  (let* ((path (get-gallery-link link)))
    (multiple-value-bind (img-id host-id) (get-img-id path)
      (get-img-metadata img-id host-id))))

(defun crawl-gallery (links)
  (let ((ch (lparallel:make-channel :fixed-capacity *fixed-capacity*)))
    (dolist (link links)
      (lparallel:submit-task ch #'get-hentai-metadata link))
    (loop for num = (length links) then (1- num) while (> num 0) do
         (let ((c (lparallel:receive-result ch)))
           (download-imgs c)))))

(defun quit (&optional code)
  ;; http://www.cliki.net/portable%20exit
  ;; This group from "clocc-port/ext.lisp"
  #+allegro (excl:exit code)
  #+clisp (#+lisp=cl ext:quit #-lisp=cl lisp:quit code)
  #+cmu (ext:quit code)
  #+ecl (si:quit)
  #+sbcl (sb-ext:exit
          :code (typecase code (number code) (null 0) (t 1))
          :abort t)
  #+ccl (ccl::quit code)
  #-(or allegro clisp cmu ecl sbcl ccl)
  (error 'not-implemented :proc (list 'quit code))
  )

(defun show-help ()
  (format t "
optional arguments:
    -h, --help                          Show this help
    -s PAGE, --start PAGE               Set start page (default: ~D)
    -e PAGE, --end PAGE                 Set end page (default: ~D)
    -o PATH, --output-path PATH         Set download path (default: ~A)
" *start* *end* *download-path*))

(defun init ()
  (let ((args
         #+allegro (cdr (system:command-line-arguments))
         #+clisp ext:*args*
         #+cmu ext:*command-line-application-arguments*
         #+ecl (cdr (member "--" (si:command-args)) :test #'string=)
         #+sbcl (cdr sb-ext:*posix-argv*)
         #+ccl (cdr (member "--" ccl:*command-line-argument-list* :test #'string=))
         #-(or allegro clisp cmu ecl sbcl ccl)
         (error 'not-implemented :proc (list 'quit code))
         ))
    (optima:multiple-value-match
        (getopt:getopt args '(("start" :required)
                              ("end" :required)
                              ("output-path" :required)
                              ("help" :nonw)))
      ((_ options nil)
       (awhen (alexandria:assoc-value options "start" :test #'string=)
         (setf *start* it))
       (awhen (alexandria:assoc-value options "end" :test #'string=)
         (setf *end* it))
       (awhen (alexandria:assoc-value options "output-path" :test #'string=)
         (setf *download-path* (make-pathname :directory it)))
       (when (assoc "help" options :test #'string=)
         (show-help)
         (quit 0)))
      ((_ _ illegals)
       (log:error (format nil "Illegal option(s):~{ ~A~}" illegals))
       (quit 1)))))

@export
(defun main ()
  (init)
  (let ((ch (lparallel:make-channel :fixed-capacity *fixed-capacity*)))
    (loop for i fixnum from *start* to *end* do
         (lparallel:submit-task ch #'get-hentai-links i))
    (loop for num = (- *end* *start*) then (1- num) while (>= num 0) do
         (let ((c (lparallel:receive-result ch)))
           (crawl-gallery c)))))
