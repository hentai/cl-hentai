#|
This file is a part of cl-hentai project.
Copyright (c) 2013 Gar Sue (labs.garsue@gmail.com)
|#

#|
Author: Gar Sue (labs.garsue@gmail.com)
|#

(in-package :cl-user)
(defpackage cl-hentai-run-asd
  (:use :cl :asdf))
(in-package :cl-hentai-run-asd)

(defsystem cl-hentai-run
    :author "Gar Sue"
    :license "LLGPL"
    :depends-on (:cl-hentai)
    :components ((:module "src"
                          :components
                          ((:file "runner"))))
    :perform (load-op :after (op c) (asdf:clear-system c)))
