# Cl-Hentai
Hentai4.me crawler.
*Currently only support Clozure CL.*

## Usage
When you have executable binary, just run like below.

    ./cl-hentai-macosx-0.1.0

When you want to run without binary, evaluate `(ql:quickload :cl-hentai-run)` like following commands.

The first time run, maybe Take some time because of downloading libraries.


For Clozure CL.

    ccl --eval '(ql:quickload :cl-hentai-run)'

For SBCL. (*Maybe this isn't work on OS X and Windows.*)

    sbcl --eval '(ql:quickload :cl-hentai-run)'


### Options
Command line options correspond with some special variables.

*Note that you must put "--" before first option if you use Clozure CL or executable binary.*

e.g.

    ./cl-hentai-macosx-0.1.0 -- --start 10 --end 20
    ccl --eval '(ql:quickload :cl-hentai-run)' -- --start 10 --end 20

Available options:

    -h, --help                   Show help
    -s PAGE, --start PAGE        Set start page (default: 1 variable: hentai.core:*start*)
    -e PAGE, --end PAGE          Set end page (default: 1 variable: hentai.core:*end*              
    -o PATH, --output-path PATH  Set download path (default: $HOME/.hentai variable:hentai.crawler:*download-path*)

When you are in REPL, you can change default value by setting another value to these special variables.

## Dependencies

    :depends-on (:cl-annot
                 :optima.ppcre
                 :alexandria
                 :anaphora
                 :log4cl
                 :drakma
                 :fast-io
                 :xpath
                 :cxml-stp
                 :yason
                 :do-urlencode
                 :getopt)

## Installation
Clone this repository into `<YOUR-QUICKLISP-LOCATION>/local-projects` .

    cd <YOUR-QUICKLISP-LOCATION>/local-projects
    git clone https://bitbucket.org/hentai/cl-hentai.git

## Author
Gar Sue (labs.garsue@gmail.com)

# Copyright
Copyright (c) 2013 Gar Sue (labs.garsue@gmail.com)

# License
Licensed under the LLGPL License.
