#|
This file is a part of cl-hentai project.
Copyright (c) 2013 Gar Sue (labs.garsue@gmail.com)
|#

(in-package :cl-user)
(defpackage hentai.crawler.test
  (:use :cl
        :5am))
(in-package :hentai.crawler.test)

;; Setup unittest logger
(log:config :debug :daily #p"./log/cl-hentai-test-%Y%m%d.log")
(log:debug "======================== UNITTEST ========================")

(test get-download-path
  "get-download-path test (private function)"
  (let ((actual (hentai.crawler::get-download-path #'user-homedir-pathname)))
    (is (string=
          (directory-namestring #p"~/.hentai/")
          (directory-namestring actual)))))

(test get-img-id
  "get-img-id test"
  (multiple-value-bind (x y) (hentai.crawler:get-img-id "/gallery-59938-1-1.html")
    (is (string= "59938" x))
    (is (string= "1" y))))

(test get-img-id-error
  "get-img-id error test"
  (multiple-value-bind (x y) (hentai.crawler:get-img-id "invalid-text")
    (is (string= "" x))
    (is (string= "" y))))

(test create-filename
  "create-filename test (private function)"
  (let* ((hentai.crawler:*download-path* #p"./create-filename-test/")
         (metadata (alexandria:alist-hash-table'((:folder-link . 123456))))
         (actual (hentai.crawler::create-filename metadata "10-hentai4.me-11.jpg")))
    (is (string= (write-to-string #p"./create-filename-test/123456/11.jpg")
                 (write-to-string actual)))
    (ensure-directories-exist hentai.crawler:*download-path*)
    #+ccl (ccl:delete-directory hentai.crawler:*download-path*)
    ))

(test create-filename-error-invalid-filename
  "create-filename test error with invalid filename (private function)"
  (let* ((hentai.crawler:*download-path* #p"./create-filename-test/")
         (metadata (alexandria:alist-hash-table '((:folder-link . 123456))))
         (actual (hentai.crawler::create-filename metadata "10-hentai4.me-XX.jpg")))
    (is (null actual))
    (ensure-directories-exist hentai.crawler:*download-path*)
    #+ccl (ccl:delete-directory hentai.crawler:*download-path*)))

(test create-filename-error-wild-pathname
  "create-filename test with wild-pathname (private function)"
  (let* ((hentai.crawler:*download-path* #p"./create-filename-test/")
         (metadata (alexandria:alist-hash-table '((:folder-link . "*"))))
         (actual (hentai.crawler::create-filename metadata "10-hentai4.me-11.jpg")))
    (is (null actual))
    (ensure-directories-exist hentai.crawler:*download-path*)
    #+ccl (ccl:delete-directory hentai.crawler:*download-path*)))

(test get-hentai-links
  "get-hentai-links test"
  (let* ((hentai.crawler:*http-request*
           (lambda (url-str)
             (values
               (concatenate
                 'string
                 "<html>"
                 "<a rel='bookmark' href='http://example.com/'>&raquo; example.com</a>"
                 "<a rel='bookmark' href='http://example2.com/'>&#187; example2.com</a>"
                 "</html>") 200 nil nil nil nil nil)))
         (actual
           (hentai.crawler:get-hentai-links 12)))
    (print hentai.crawler:*http-request*)
    (is (equal '("http://example.com/" "http://example2.com/") actual))))

(test get-hentai-links-error
  "get-hentai-links error test"
  (let* ((hentai.crawler:*http-request*
           (lambda (url-str) (error "Hentai Error: ~S" url-str)))
         (actual (hentai.crawler:get-hentai-links 12)))
    (is (null actual))))

(test get-gallery-link
  "get-gallery-link test"
  (let* ((hentai.crawler:*http-request*
           (lambda (url-str)
             (values
               (concatenate
                 'string
                 "<html>"
                 "<a title='View Gallery' href='http://example.com/'>example.com</a>"
                 " <a title='View Gallery' href='http://example2.com/'>example2.com</a>"
                 " </html>") 200 nil nil nil nil nil)))
         (actual (hentai.crawler:get-gallery-link "http://example.com/")))
    (is (string= "http://example.com/" actual))))

(test get-gallery-link-error
  "get-gallery-link error test"
  (let* ((hentai.crawler:*http-request*
           (lambda (url-str)
             (error "Gallery Error: ~S" url-str)))
         (actual (hentai.crawler:get-gallery-link "http://example.com/")))
    (is (null actual))))

(test get-img-metadata
  "get-img-metadata test"
  :test-key ;Intern for json keys
  (let* ((hentai.crawler:*http-request*
           (lambda (url-str)
             (values
               (concatenate
                 'string
                 "eyJ0ZXN0X2tleSI6IFsidGVzdF92YWx1ZV8xIiwgInRlc3Rfdm"
                 "FsdWVfMiJdfQ%3D%3D%0A")
               200 nil nil nil nil nil)))
         (actual (hentai.crawler:get-img-metadata 59938 1)))
    (is (equal '((:test-key "test_value_1" "test_value_2"))
               (alexandria:hash-table-alist actual)))))

(test get-img-metadata-error-json
  "get-img-metadata error test with broken json"
  :test-key ;Intern for json keys
  (let* ((hentai.crawler:*http-request*
           (lambda (url-str)
             (values
               (concatenate
                 'string
                 ;;Broken json => {"test_key": ["test_value_1", "tes
                 "eyJ0ZXN0X2tleSI6IFsidGVzdF92YWx1ZV8xIiwgInRlcw%3D%3D%0A")
               200 nil nil nil nil nil)))
         (actual (hentai.crawler:get-img-metadata 59938 1)))
    (is (null actual))))

(test get-img
  "get-img test (private function)"
  (let* ((hentai.crawler:*http-request*
           (lambda (url-str &rest rest)
             (log:debug rest)
             (values
               (flexi-streams:string-to-octets url-str)
               200 nil nil nil nil nil)))
         (metadata (alexandria:alist-hash-table
                           '((:host . "host") (:folder-link . "folder"))))
         (actual (hentai.crawler::get-img metadata "img-name.jpg" (vector)))
         (expected (length "http://host.hdporn4.me/folder/img-name.jpg")))
    (is (equal expected actual))))

(run!)
