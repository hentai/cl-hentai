(ql:quickload :getopt)
(ql:quickload :optima)
(ql:quickload :alexandria)

(let ((args
       #+ccl (cdr (member "--" ccl:*command-line-argument-list* :test #'string=))))
  (optima:multiple-value-match
      (getopt:getopt args '(("start" :required)
                            ("end" :required)
                            ("output-path" :required)
                            ("help" :none)
                            ))
    ((_ options nil)
     (format t "options: ~A~%" options)
     (format t "start: ~A~%" (alexandria:assoc-value options "start" :test #'string=))
     (format t "end: ~A~%" (alexandria:assoc-value options "end" :test #'string=))
     (format t "output-path: ~A~%"
             ;; (make-pathname
             ;;  :directory
             ;;  (alexandria:assoc-value options "output-path" :test #'string=))
              (alexandria:assoc-value options "output-path" :test #'string=)
             )
     (format t "help: ~A~%" (assoc "help" options :test #'string=))
     (when (assoc "help" options :test #'string=)
       (princ "optional arguments:
    -h, --help                          Show help
    -s PAGE, --start PAGE               Set start page (default 1)
    -e PAGE, --end PAGE                 Set end page (default 1)
    -o PATH, --output-path PATH         Set download path (~/.hentai)
")))
    ((_ _ illegals)
     (format t "Illegal option(s):~{ ~A~}" illegals))))

(quit)
