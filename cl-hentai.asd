#|
This file is a part of cl-hentai project.
Copyright (c) 2013 Gar Sue (labs.garsue@gmail.com)
|#

#|
Author: Gar Sue (labs.garsue@gmail.com)
|#

(in-package :cl-user)
(defpackage cl-hentai-asd
  (:use :cl :asdf))
(in-package :cl-hentai-asd)

(defsystem cl-hentai
  :version "0.1"
  :author "Gar Sue"
  :license "LLGPL"
  :depends-on (:cl-annot
               :optima.ppcre
               :alexandria
               :anaphora
               :log4cl
               :drakma
               :fast-io
               :xpath
               :cxml-stp
               :yason
               :do-urlencode
               :getopt
               :lparallel)
  :components ((:module "src"
                        :components
                        ((:file "resources")
                         (:file "crawler" :depends-on ("resources"))
                         (:file "core" :depends-on ("crawler")))))
  :description ""
  :long-description
  #.(with-open-file (stream (merge-pathnames
                             #p"README.md"
                             (or *load-pathname* *compile-file-pathname*))
                            :if-does-not-exist nil
                            :direction :input)
                    (when stream
                      (let ((seq (make-array (file-length stream)
                                             :element-type 'character
                                             :fill-pointer t)))
                        (setf (fill-pointer seq) (read-sequence seq stream))
                        seq)))
  :in-order-to ((test-op (load-op cl-hentai-test))))
